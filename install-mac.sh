#!/bin/bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

brew install node
brew install python3
brew install pip
brew install neovim
pip3 install neovim

mv -f ~/.config/nvim/init.vim ~/.config/nvim/init.vim.bkp
cp ./init.vim ~/.config/nvim/init.vim
