let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

set wildmode=longest,list,full
set wildmenu
set wildignore+=**/.git/*

set number
set relativenumber
set encoding=utf-8
set expandtab
set clipboard+=unnamedplus
set directory^=$HOME/tempswap//
set nocompatible
set guicursor=
set nohlsearch
set hidden
set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu
set nowrap
set noswapfile
set nobackup
set incsearch
set termguicolors
set scrolloff=8
set noshowmode
set signcolumn=yes
set isfname+=@-@

" Give more space for displaying messages.
set cmdheight=1

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=50

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

let mapleader = " "
let g:nerdtree_sync_cursorline = 1


call plug#begin()
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'preservim/nerdtree'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'gruvbox-community/gruvbox'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'sheerun/vim-polyglot'
Plug 'dense-analysis/ale'
Plug 'jiangmiao/auto-pairs'
Plug 'vim-airline/vim-airline'
Plug 'ervandew/supertab'
Plug 'puremourning/vimspector'
Plug 'prettier/vim-prettier', { 'do': 'npm install' , 'branch' : 'release/1.x' }
Plug 'valloric/MatchTagAlways'
call plug#end()

let g:coc_global_extensions=[ 'coc-omnisharp']

scriptencoding utf-8
colorscheme gruvbox
filetype plugin on
syntax on

" General Mappings
:nnoremap <C-n> :NERDTreeToggle<CR>
:nnoremap <Leader><C-f> :Files<CR>
:nnoremap <Leader>b :buffers<CR>
:nnoremap <S-Right> $
:nnoremap <S-Left> 0

" Tabs
nnoremap <Leader><Right> :tabnext<CR>
nnoremap <Leader><Left> :tabprevious<CR>
nnoremap <Leader><Up> :tabnew<CR>
nnoremap <Leader><Down> :tabclose<CR>

" Coc Mappings

nnoremap <Leader>gd :call CocActionAsync('jumpDefinition')<CR>
nnoremap <Leader>fr :call CocActionAsync('jumpReferences')<CR> 
nnoremap <Leader>fi :call CocActionAsync('findImplementations')<CR> 
nnoremap <Leader>fs :<C-u>CocList -I symbols<CR> 
nnoremap <Leader>r :call CocActionAsync('rename')<CR>
nnoremap <Leader>. :call CocActionAsync('codeAction')<CR>
nnoremap <Leader>f :call CocActionAsync('format')<CR>

command EditConfig edit ~/.config/nvim/init.vim
