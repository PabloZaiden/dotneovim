#!/bin/bash

sudo curl -fsSL https://deb.nodesource.com/setup_17.x | bash -
sudo apt install -y wget curl git python3 pip build-essential nodejs
pip3 install neovim

mv -f ~/.config/nvim/init.vim ~/.config/nvim/init.vim.bkp
cp ./init.vim ~/.config/nvim/init.vim

wget https://github.com/neovim/neovim/releases/download/v0.6.1/nvim.appimage
chmod a+x ./nvim.appimage
mv ./nvim.appimage /usr/local/bin/nvim

