FROM mcr.microsoft.com/dotnet/sdk

RUN apt update && apt install -y sudo

RUN mkdir -p /root/.config/nvim 

WORKDIR /

COPY ./init.vim /
COPY ./install-linux.sh /

RUN /install-linux.sh
RUN nvim --appimage-extract-and-run --headless +PlugInstall +qall 

RUN mkdir /source
WORKDIR /source

ENTRYPOINT "/usr/local/bin/nvim" "--appimage-extract-and-run" "/source"
