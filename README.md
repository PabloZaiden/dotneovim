# DotNeoVim

dotnet dev environment with neovim

## Usage

- Install dotnet
- Install node
- Install python3 and pip3
- Install python3 support for neovim with: `pip3 install neovim`
- Copy `init.vim` to `~/.config/nvim/init.vim` 

## Useful shortcuts

- `<Ctrl> n`: Toggle NerdTree
- `<Leader><Ctrl> f`: Find file
- `<Leader> b`: Show current buffers
- `<Leader>f`: Format
- `<Leader>gd`: Go to definition
- `<Leader>fr`: Find references
- `<Leader>fi`: Find implementations
- `<Leader>fs`: Find symbol
- `<Leader>r`: Rename 
- `<Leader>.`: Code actions
