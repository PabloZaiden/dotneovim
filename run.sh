#!/bin/sh

DIR=$(pwd)/$1
if [ -z "$1" ]
then
	DIR=$(pwd)
fi

docker run --rm -ti -v "$DIR":/source dotneovim 
